package downloader

import "net/url"

func Download(uri string) string {
	return url.PathEscape(uri)
}
